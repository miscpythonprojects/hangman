# Hangman

## What is this project about

This small code is to run handman game. It will help understand loops and conditions 

## Add your files

- Words list: I am using the following code to access a list of words online. It was giving a 'b' at the start of each word. So I copy/pasted the file from the url mentioned and then read that file rather than going online all the time


'''
import requests, random

word_site = "https://www.mit.edu/~ecprice/wordlist.10000"
response = requests.get(word_site)
WORDS = response.content.splitlines()

word_picked = random.choice(WORDS)
'''
