# Python 3
# coding: utf-8

__author__ = "Ammar S Malik"
__copyright__ = "Copyright 2022, API call to IMDB"
__credits__ = ["Ammar Malik"]
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "all creditors/github"
__email__ = "dev.malik.ammar@gmail.com"
__status__ = "Production"

import random, HangmanArt, os
blankChar = "❓"

# function to show each character on the screen filled or blank
def showStatus(lst_Display) -> str:
    toReturn = ""
    for char in lst_Display:
        if char == "_":
            toReturn += blankChar
        else:
            toReturn += char
    return toReturn

# See if we still have a character missing. It will return a false on the first find
def checkInComplete(lst_Display):
    for char in lst_Display:
        if char == "_":
            return False
    return True

os.system('clear')

# read a text file to choose a word from
word_file = "WordsList.txt"
WORDS = open(word_file).read().splitlines()
word_picked = random.choice(WORDS)

print(HangmanArt._Hangman)

# We have picked the word from the list. Now show as many blocks on the screen as there are words 
displayBlock = []
for count in range(len(word_picked)):
    displayBlock += "_"

print(f"Word picked up was : {word_picked}")
print("Guess the word and save him !!!!\n\n\n")
# print("Guess what the word is : " + showStatus(displayBlock))
inputTries = 0
gameOver = False
lives = 5
while not gameOver:
    # Get a character from the user. It should be one character
    charInput = ""
    inputTries = 0
    while len(charInput) <= 0 or len(charInput) > 1:
        charInput = input("Please enter a character : ")
        inputTries += 1
        if inputTries > 3:
            gameOver = True
            charInput = "-"

    if gameOver:
        print("You entered invalid characters multiple times. You loose!!!")
    else:
        # a valid character was added to the list. See if we have this character in the word or not
        lcharWasFound = False
        for position in range(len(word_picked)):
            charInWord = word_picked[position]
            if charInWord == charInput:
                displayBlock[position] = charInWord
                lcharWasFound = True

        # now check if the character was found or not
        if not lcharWasFound:
            lives -= 1

        # now show the hangman status
        os.system('clear')
        print(HangmanArt._Hangman)
        print(HangmanArt._Hangman_Lives[lives])
        print("Guess what the word is : " + showStatus(displayBlock))

        if lives <= 0:
            gameOver = True
        else:
            # Also check if all characters are found 
            gameOver = checkInComplete(displayBlock)


if lives <= 0:
    print("Sorry. Try next time")
else:
    print("Wow. You wor !!!")